import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moodio/Views/Widgets/view_all.dart';

import '../../constants/urls.dart';
import '../../models/home/home_data.dart';

class NewestMusic extends StatelessWidget {
  final List<MostLikedMusics> mostLikedMusic;

  const NewestMusic({super.key, required this.mostLikedMusic});

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(24),
          child: Row(
            children: [
              const ViewAll(
                text: 'مشاهده همه',
              ),
              Text(
                'جدید ترین های پاپ',
                style: themeData.textTheme.headline6,
              ),
            ],
          ),
        ),
        SizedBox(
            height: MediaQuery.of(context).size.height / 4.5,
            child: ListView.builder(
              physics: const BouncingScrollPhysics(),
              reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: mostLikedMusic.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: index == 0 || index == mostLikedMusic.length
                      ? const EdgeInsets.only(right: 24, left: 24)
                      : const EdgeInsets.only(left: 24),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.height / 7,
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.network(AppUrls.photosBaseURL +
                              mostLikedMusic[index].mobilePoster),
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              mostLikedMusic[index].nameFa,
                              style: themeData.textTheme.bodyText1,
                            )),
                        Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              mostLikedMusic[index].artists[0].nameFa,
                              style: themeData.textTheme.bodyText2,
                            )),
                      ],
                    ),
                  ),
                );
              },
            ))
      ],
    );
  }
}
