import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moodio/controllers/home_controller.dart';
import 'package:moodio/controllers/main_controllers.dart';

class buttomNavigation extends StatelessWidget {
  final MainController mainController;
  final int selectedIndex;
  final Function(int index) onTap;

  const buttomNavigation(
      {super.key,
      required this.selectedIndex,
      required this.mainController,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.grey.shade700,
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BottomNavigationItem(
              iconFileName: Icons.home,
              activeIconFileName: 'home',
              title: 'home',
              onTap: () {
                onTap(0);
              },
              isActive: selectedIndex == 0,
            ),
            BottomNavigationItem(
              iconFileName: Icons.play_circle,
              activeIconFileName: 'play',
              title: 'play',
              onTap: () {
                onTap(1);
              },
              isActive: selectedIndex == 1,
            ),
            BottomNavigationItem(
              iconFileName: Icons.search,
              activeIconFileName: 'search',
              title: 'search',
              onTap: () {
                onTap(2);
              },
              isActive: selectedIndex == 2,
            ),
            BottomNavigationItem(
              iconFileName: Icons.call,
              activeIconFileName: 'call',
              title: 'call',
              onTap: () {
                onTap(3);
              },
              isActive: selectedIndex == 3,
            ),
            BottomNavigationItem(
              iconFileName: Icons.person,
              activeIconFileName: 'person',
              title: 'person',
              onTap: () {
                onTap(4);
              },
              isActive: selectedIndex == 4,
            ),
          ],
        ),
      ),
    );
  }
}

class BottomNavigationItem extends StatelessWidget {
  final IconData iconFileName;
  final String activeIconFileName;
  final String title;
  final bool isActive;
  final Function() onTap;

  const BottomNavigationItem(
      {super.key,
      required this.iconFileName,
      required this.activeIconFileName,
      required this.title,
      required this.isActive,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 500),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: isActive ? Colors.grey.shade600 : Colors.grey.shade700),
        curve: Curves.easeIn,
        child: isActive
            ? Padding(
          padding: EdgeInsets.all(8),
              child: Row(
                  children: [
                    Icon(iconFileName),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      title,
                      style: const TextStyle(color: Colors.white38),
                    ),
                    const SizedBox(
                      width: 4,
                    )
                  ],
                ),
            )
            : SizedBox(
                child: Icon(
                iconFileName,
                color: Colors.black54,
              )),
      ),
    );
  }
}
