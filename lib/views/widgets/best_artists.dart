import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moodio/constants/urls.dart';

import '../../models/home/home_data.dart';

class BestArtist extends StatelessWidget {
  final List<BestArtists> bestArtists;

  const BestArtist({super.key, required this.bestArtists});

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(24),
          child: Align(
            alignment: Alignment.topRight,
            child: Text(
              'خواننده های پرطرفدار',
              style: themeData.textTheme.headline6,
            ),
          ),
        ),
        const SizedBox(
          height: 12,
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height / 2,
          child: GridView.builder(
            physics: const BouncingScrollPhysics(),
            reverse: true,
            scrollDirection: Axis.horizontal,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 1.3,
              crossAxisSpacing: 6,
            ),
            itemCount: bestArtists.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  Padding(
                    padding: index == 0 || index == 1
                        ? const EdgeInsets.only(right: 12, left: 12)
                        : const EdgeInsets.only(left: 12),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.network(
                        bestArtists[index].mobilePoster != ""
                            ? AppUrls.photosBaseURL +
                                bestArtists[index].mobilePoster
                            : AppUrls.photosBaseURL +
                                bestArtists[1].mobilePoster,
                        fit: BoxFit.fill,
                        width: MediaQuery.of(context).size.width / 3.5,
                        height: MediaQuery.of(context).size.width / 3.5,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Flexible(
                        child: Text(
                      bestArtists[index].nameFa,
                      style: themeData.textTheme.bodyText1,
                      overflow: TextOverflow.ellipsis,
                    )),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('دنبال کردن');
                      print(
                          AppUrls.photosBaseURL + bestArtists[3].mobilePoster);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: const LinearGradient(
                          colors: [Colors.pink, Colors.purple],
                        ),
                      ),
                      child: const Align(
                          alignment: Alignment.center,
                          child: Text('دنبال کردن')),
                    ),
                  ),
                ],
              );
            },
          ),
        )
      ],
    );
  }
}
