import 'package:flutter/cupertino.dart';
import 'package:moodio/constants/urls.dart';

import '../../models/home/home_data.dart';

class TopPlayList extends StatelessWidget {
  final List<TopPlaylist> topPlaylist;

  const TopPlayList({super.key, required this.topPlaylist});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 3,
      child: Padding(
        padding: EdgeInsets.all(24),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 12),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(24),
                child: Image.network(
                  AppUrls.photosBaseURL + topPlaylist[0].mobilePoster,
                  width: MediaQuery.of(context).size.width / 1.7,
                  height: MediaQuery.of(context).size.height / 3,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 6),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(24),
                    child: Image.network(
                      AppUrls.photosBaseURL + topPlaylist[1].mobilePoster,
                      width: MediaQuery.of(context).size.width / 3 - 24,
                      height: MediaQuery.of(context).size.height / 6 - 6 - 24,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 6),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(24),
                    child: Image.network(
                      AppUrls.photosBaseURL + topPlaylist[2].mobilePoster,
                      width: MediaQuery.of(context).size.width / 3 - 24,
                      height: MediaQuery.of(context).size.height / 6 - 6 - 24,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
