import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moodio/controllers/home_controller.dart';
import 'package:moodio/views/widgets/best_artists.dart';
import 'package:moodio/views/widgets/newest_music.dart';
import 'package:moodio/views/widgets/newest_pop.dart';
import 'package:moodio/views/widgets/top_play_list.dart';

import '../Widgets/view_all.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    final homeController = Get.find<HomeController>();
    return Scaffold(
      body: Obx(() {
        if (homeController.isLoading.value) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return SafeArea(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                ViewAllButton(),
                Divider(),
                NewPop(
                  newestMusic: homeController.data.value.data.newestMusic,
                ),
                BestArtist(
                    bestArtists: homeController.data.value.data.bestArtists),
                NewestMusic(
                    mostLikedMusic:
                        homeController.data.value.data.mostLikedMusics),
                TopPlayList(
                    topPlaylist: homeController.data.value.data.topPlaylist)
              ],
            ),
          ),
        );
      }),
    );
  }
}
