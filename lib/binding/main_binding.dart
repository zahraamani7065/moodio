import 'package:get/get.dart';
import 'package:moodio/controllers/main_controllers.dart';

class MainBinding extends Bindings{
  @override
  void dependencies() {
  Get.lazyPut(() => MainController());

  }

}