class AutoGenerate {
  AutoGenerate(
  );
  late final String successMessage;
  late final String errorMessage;
  late final int statusCode;
  late final Data data;
  late final Metadata metadata;

  AutoGenerate.fromJson(Map<String, dynamic> json){
    successMessage = json['success_message'];
    errorMessage = json['error_message'];
    statusCode = json['status_code'];
    data = Data.fromJson(json['data']);
    metadata = Metadata.fromJson(json['metadata']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['success_message'] = successMessage;
    _data['error_message'] = errorMessage;
    _data['status_code'] = statusCode;
    _data['data'] = data.toJson();
    _data['metadata'] = metadata.toJson();
    return _data;
  }

}

class Data {
  Data({
    required this.activePrediction,
    required this.bestArtists,
    required this.customMusic,
    required this.ethnicPlaylists,
    required this.locationPlaylist,
    required this.mostLikedMusics,
    required this.newestMusic,
    required this.recentlyPlayed,
    required this.tags,
    required this.topPlaylist,
  });
  late final bool activePrediction;
  late final List<BestArtists> bestArtists;
  late final CustomMusic customMusic;
  late final List<EthnicPlaylists> ethnicPlaylists;
  late final List<LocationPlaylist> locationPlaylist;
  late final List<MostLikedMusics> mostLikedMusics;
  late final List<NewestMusic> newestMusic;
  late final List<dynamic> recentlyPlayed;
  late final List<Tags> tags;
  late final List<TopPlaylist> topPlaylist;

  Data.fromJson(Map<String, dynamic> json){
    activePrediction = json['active_prediction'];
    bestArtists = List.from(json['best_artists']).map((e)=>BestArtists.fromJson(e)).toList();
    customMusic = CustomMusic.fromJson(json['custom_music']);
    ethnicPlaylists = List.from(json['ethnic_playlists']).map((e)=>EthnicPlaylists.fromJson(e)).toList();
    locationPlaylist = List.from(json['location_playlist']).map((e)=>LocationPlaylist.fromJson(e)).toList();
    mostLikedMusics = List.from(json['most_liked_musics']).map((e)=>MostLikedMusics.fromJson(e)).toList();
    newestMusic = List.from(json['newest_music']).map((e)=>NewestMusic.fromJson(e)).toList();
    recentlyPlayed = List.castFrom<dynamic, dynamic>(json['recently_played']);
    tags = List.from(json['tags']).map((e)=>Tags.fromJson(e)).toList();
    topPlaylist = List.from(json['top_playlist']).map((e)=>TopPlaylist.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['active_prediction'] = activePrediction;
    _data['best_artists'] = bestArtists.map((e)=>e.toJson()).toList();
    _data['custom_music'] = customMusic.toJson();
    _data['ethnic_playlists'] = ethnicPlaylists.map((e)=>e.toJson()).toList();
    _data['location_playlist'] = locationPlaylist.map((e)=>e.toJson()).toList();
    _data['most_liked_musics'] = mostLikedMusics.map((e)=>e.toJson()).toList();
    _data['newest_music'] = newestMusic.map((e)=>e.toJson()).toList();
    _data['recently_played'] = recentlyPlayed;
    _data['tags'] = tags.map((e)=>e.toJson()).toList();
    _data['top_playlist'] = topPlaylist.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class BestArtists {
  BestArtists({
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.description,
    required this.avatar,
    required this.banner,
    required this.desktopPoster,
    required this.mobilePoster,
    required this.biography,
    required this.followingCounts,
    required this.musicCounts,
    required this.followed,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
    required this.createdAt,
  });
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final String description;
  late final String avatar;
  late final String banner;
  late final String desktopPoster;
  late final String mobilePoster;
  late final String biography;
  late final int followingCounts;
  late final int musicCounts;
  late final bool followed;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;
  late final String createdAt;

  BestArtists.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    description = json['description'];
    avatar = json['avatar'];
    banner = json['banner'];
    desktopPoster = json['desktop_poster'];
    mobilePoster = json['mobile_poster'];
    biography = json['biography'];
    followingCounts = json['following_counts'];
    musicCounts = json['music_counts'];
    followed = json['followed'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['description'] = description;
    _data['avatar'] = avatar;
    _data['banner'] = banner;
    _data['desktop_poster'] = desktopPoster;
    _data['mobile_poster'] = mobilePoster;
    _data['biography'] = biography;
    _data['following_counts'] = followingCounts;
    _data['music_counts'] = musicCounts;
    _data['followed'] = followed;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class CustomMusic {
  CustomMusic({
    required this.category,
    required this.label,
    required this.musics,
    required this.title,
  });
  late final String category;
  late final String label;
  late final List<Musics> musics;
  late final String title;

  CustomMusic.fromJson(Map<String, dynamic> json){
    category = json['category'];
    label = json['label'];
    musics = List.from(json['musics']).map((e)=>Musics.fromJson(e)).toList();
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['category'] = category;
    _data['label'] = label;
    _data['musics'] = musics.map((e)=>e.toJson()).toList();
    _data['title'] = title;
    return _data;
  }
}

class Musics {
  Musics({
    required this.addToPlaylist,
    required this.artists,
    required this.banner,
    required this.categories,
    required this.company,
    required this.createdAt,
    required this.description,
    required this.desktopPoster,
    required this.dislikeCount,
    required this.disliked,
    required this.followers,
    required this.id,
    required this.information,
    required this.labels,
    required this.likeCount,
    required this.liked,
    required this.marked,
    required this.mobilePoster,
    required this.nameEn,
    required this.nameFa,
    required this.normalRbts,
    required this.playCount,
    required this.poem,
    required this.published,
    required this.publishedAt,
    required this.streamPath,
    required this.tags,
    required this.updatedAt,
  });
  late final bool addToPlaylist;
  late final List<Artists> artists;
  late final String banner;
  late final List<Categories> categories;
  late final Company company;
  late final String createdAt;
  late final String description;
  late final String desktopPoster;
  late final int dislikeCount;
  late final bool disliked;
  late final int followers;
  late final String id;
  late final List<dynamic> information;
  late final List<String> labels;
  late final int likeCount;
  late final bool liked;
  late final bool marked;
  late final String mobilePoster;
  late final String nameEn;
  late final String nameFa;
  late final List<NormalRbts> normalRbts;
  late final int playCount;
  late final String poem;
  late final bool published;
  late final String publishedAt;
  late final String streamPath;
  late final List<Tags> tags;
  late final String updatedAt;

  Musics.fromJson(Map<String, dynamic> json){
    addToPlaylist = json['add_to_playlist'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    banner = json['banner'];
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    company = Company.fromJson(json['company']);
    createdAt = json['created_at'];
    description = json['description'];
    desktopPoster = json['desktop_poster'];
    dislikeCount = json['dislike_count'];
    disliked = json['disliked'];
    followers = json['followers'];
    id = json['id'];
    information = List.castFrom<dynamic, dynamic>(json['information']);
    labels = List.castFrom<dynamic, String>(json['labels']);
    likeCount = json['like_count'];
    liked = json['liked'];
    marked = json['marked'];
    mobilePoster = json['mobile_poster'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    normalRbts = List.from(json['normal_rbts']).map((e)=>NormalRbts.fromJson(e)).toList();
    playCount = json['play_count'];
    poem = json['poem'];
    published = json['published'];
    publishedAt = json['published_at'];
    streamPath = json['stream_path'];
    tags = List.from(json['tags']).map((e)=>Tags.fromJson(e)).toList();
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_to_playlist'] = addToPlaylist;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['banner'] = banner;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['company'] = company.toJson();
    _data['created_at'] = createdAt;
    _data['description'] = description;
    _data['desktop_poster'] = desktopPoster;
    _data['dislike_count'] = dislikeCount;
    _data['disliked'] = disliked;
    _data['followers'] = followers;
    _data['id'] = id;
    _data['information'] = information;
    _data['labels'] = labels;
    _data['like_count'] = likeCount;
    _data['liked'] = liked;
    _data['marked'] = marked;
    _data['mobile_poster'] = mobilePoster;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['normal_rbts'] = normalRbts.map((e)=>e.toJson()).toList();
    _data['play_count'] = playCount;
    _data['poem'] = poem;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['stream_path'] = streamPath;
    _data['tags'] = tags.map((e)=>e.toJson()).toList();
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class Artists {
  Artists({
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.description,
    required this.avatar,
    required this.banner,
    required this.desktopPoster,
    required this.mobilePoster,
    required this.biography,
    required this.followingCounts,
    required this.musicCounts,
    required this.followed,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
    required this.createdAt,
  });
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final String description;
  late final String avatar;
  late final String banner;
  late final String desktopPoster;
  late final String mobilePoster;
  late final String biography;
  late final int followingCounts;
  late final int musicCounts;
  late final bool followed;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;
  late final String createdAt;

  Artists.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    description = json['description'];
    avatar = json['avatar'];
    banner = json['banner'];
    desktopPoster = json['desktop_poster'];
    mobilePoster = json['mobile_poster'];
    biography = json['biography'];
    followingCounts = json['following_counts'];
    musicCounts = json['music_counts'];
    followed = json['followed'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['description'] = description;
    _data['avatar'] = avatar;
    _data['banner'] = banner;
    _data['desktop_poster'] = desktopPoster;
    _data['mobile_poster'] = mobilePoster;
    _data['biography'] = biography;
    _data['following_counts'] = followingCounts;
    _data['music_counts'] = musicCounts;
    _data['followed'] = followed;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class Categories {
  Categories({
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.description,
    required this.banner,
    required this.desktopPoster,
    required this.mobilePoster,
    required this.group,
    this.labels,
    required this.followed,
    required this.published,
    required this.isParent,
    required this.musicCounts,
    required this.parent,
    required this.publishedAt,
    required this.updatedAt,
    required this.createdAt,
  });
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final String description;
  late final String banner;
  late final String desktopPoster;
  late final String mobilePoster;
  late final String group;
  late final Null labels;
  late final bool followed;
  late final bool published;
  late final bool isParent;
  late final int musicCounts;
  late final String parent;
  late final String publishedAt;
  late final String updatedAt;
  late final String createdAt;

  Categories.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    description = json['description'];
    banner = json['banner'];
    desktopPoster = json['desktop_poster'];
    mobilePoster = json['mobile_poster'];
    group = json['group'];
    labels = null;
    followed = json['followed'];
    published = json['published'];
    isParent = json['is_parent'];
    musicCounts = json['music_counts'];
    parent = json['parent'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['description'] = description;
    _data['banner'] = banner;
    _data['desktop_poster'] = desktopPoster;
    _data['mobile_poster'] = mobilePoster;
    _data['group'] = group;
    _data['labels'] = labels;
    _data['followed'] = followed;
    _data['published'] = published;
    _data['is_parent'] = isParent;
    _data['music_counts'] = musicCounts;
    _data['parent'] = parent;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class Company {
  Company({
    required this.id,
    required this.owner,
    required this.nameEn,
    required this.nameFa,
    required this.description,
    required this.avatar,
    required this.banner,
    required this.url,
    required this.api,
    required this.locked,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
    required this.createdAt,
  });
  late final String id;
  late final String owner;
  late final String nameEn;
  late final String nameFa;
  late final String description;
  late final String avatar;
  late final String banner;
  late final String url;
  late final String api;
  late final bool locked;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;
  late final String createdAt;

  Company.fromJson(Map<String, dynamic> json){
    id = json['id'];
    owner = json['owner'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    description = json['description'];
    avatar = json['avatar'];
    banner = json['banner'];
    url = json['url'];
    api = json['api'];
    locked = json['locked'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['owner'] = owner;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['description'] = description;
    _data['avatar'] = avatar;
    _data['banner'] = banner;
    _data['url'] = url;
    _data['api'] = api;
    _data['locked'] = locked;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class NormalRbts {
  NormalRbts({
    required this.artist,
    required this.categories,
    required this.createdAt,
    required this.id,
    this.labels,
    required this.marked,
    required this.music,
    required this.poster,
    required this.price,
    required this.showableName,
    required this.toncode,
    required this.tonid,
    required this.updatedAt,
    required this.url,
  });
  late final String artist;
  late final List<dynamic> categories;
  late final String createdAt;
  late final String id;
  late final Null labels;
  late final bool marked;
  late final String music;
  late final String poster;
  late final int price;
  late final String showableName;
  late final String toncode;
  late final String tonid;
  late final String updatedAt;
  late final String url;

  NormalRbts.fromJson(Map<String, dynamic> json){
    artist = json['artist'];
    categories = List.castFrom<dynamic, dynamic>(json['categories']);
    createdAt = json['created_at'];
    id = json['id'];
    labels = null;
    marked = json['marked'];
    music = json['music'];
    poster = json['poster'];
    price = json['price'];
    showableName = json['showable_name'];
    toncode = json['toncode'];
    tonid = json['tonid'];
    updatedAt = json['updated_at'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist'] = artist;
    _data['categories'] = categories;
    _data['created_at'] = createdAt;
    _data['id'] = id;
    _data['labels'] = labels;
    _data['marked'] = marked;
    _data['music'] = music;
    _data['poster'] = poster;
    _data['price'] = price;
    _data['showable_name'] = showableName;
    _data['toncode'] = toncode;
    _data['tonid'] = tonid;
    _data['updated_at'] = updatedAt;
    _data['url'] = url;
    return _data;
  }
}

class Tags {
  Tags({
    required this.id,
    required this.name,
    required this.banner,
    required this.desktopPoster,
    required this.mobilePoster,
    required this.createdAt,
  });
  late final String id;
  late final String name;
  late final String banner;
  late final String desktopPoster;
  late final String mobilePoster;
  late final String createdAt;

  Tags.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    banner = json['banner'];
    desktopPoster = json['desktop_poster'];
    mobilePoster = json['mobile_poster'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['banner'] = banner;
    _data['desktop_poster'] = desktopPoster;
    _data['mobile_poster'] = mobilePoster;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class EthnicPlaylists {
  EthnicPlaylists({
    required this.addOnPlaylist,
    required this.artist,
    this.avatars,
    required this.createdAt,
    required this.desktopPoster,
    required this.followCounts,
    required this.followed,
    required this.id,
    required this.labels,
    required this.mobilePoster,
    required this.musicCounts,
    required this.name,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
  });
  late final bool addOnPlaylist;
  late final Artist artist;
  late final Null avatars;
  late final String createdAt;
  late final String desktopPoster;
  late final int followCounts;
  late final bool followed;
  late final String id;
  late final List<String> labels;
  late final String mobilePoster;
  late final int musicCounts;
  late final String name;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;

  EthnicPlaylists.fromJson(Map<String, dynamic> json){
    addOnPlaylist = json['add_on_playlist'];
    artist = Artist.fromJson(json['artist']);
    avatars = null;
    createdAt = json['created_at'];
    desktopPoster = json['desktop_poster'];
    followCounts = json['follow_counts'];
    followed = json['followed'];
    id = json['id'];
    labels = List.castFrom<dynamic, String>(json['labels']);
    mobilePoster = json['mobile_poster'];
    musicCounts = json['music_counts'];
    name = json['name'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_on_playlist'] = addOnPlaylist;
    _data['artist'] = artist.toJson();
    _data['avatars'] = avatars;
    _data['created_at'] = createdAt;
    _data['desktop_poster'] = desktopPoster;
    _data['follow_counts'] = followCounts;
    _data['followed'] = followed;
    _data['id'] = id;
    _data['labels'] = labels;
    _data['mobile_poster'] = mobilePoster;
    _data['music_counts'] = musicCounts;
    _data['name'] = name;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class Artist {
  Artist({
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.description,
    required this.avatar,
    required this.banner,
    required this.desktopPoster,
    required this.mobilePoster,
    required this.biography,
    required this.followingCounts,
    required this.musicCounts,
    required this.followed,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
    required this.createdAt,
  });
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final String description;
  late final String avatar;
  late final String banner;
  late final String desktopPoster;
  late final String mobilePoster;
  late final String biography;
  late final int followingCounts;
  late final int musicCounts;
  late final bool followed;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;
  late final String createdAt;

  Artist.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    description = json['description'];
    avatar = json['avatar'];
    banner = json['banner'];
    desktopPoster = json['desktop_poster'];
    mobilePoster = json['mobile_poster'];
    biography = json['biography'];
    followingCounts = json['following_counts'];
    musicCounts = json['music_counts'];
    followed = json['followed'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['description'] = description;
    _data['avatar'] = avatar;
    _data['banner'] = banner;
    _data['desktop_poster'] = desktopPoster;
    _data['mobile_poster'] = mobilePoster;
    _data['biography'] = biography;
    _data['following_counts'] = followingCounts;
    _data['music_counts'] = musicCounts;
    _data['followed'] = followed;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    _data['created_at'] = createdAt;
    return _data;
  }
}

class LocationPlaylist {
  LocationPlaylist({
    required this.addOnPlaylist,
    required this.artist,
    this.avatars,
    required this.createdAt,
    required this.desktopPoster,
    required this.followCounts,
    required this.followed,
    required this.id,
    required this.labels,
    required this.mobilePoster,
    required this.musicCounts,
    required this.name,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
  });
  late final bool addOnPlaylist;
  late final Artist artist;
  late final Null avatars;
  late final String createdAt;
  late final String desktopPoster;
  late final int followCounts;
  late final bool followed;
  late final String id;
  late final List<String> labels;
  late final String mobilePoster;
  late final int musicCounts;
  late final String name;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;

  LocationPlaylist.fromJson(Map<String, dynamic> json){
    addOnPlaylist = json['add_on_playlist'];
    artist = Artist.fromJson(json['artist']);
    avatars = null;
    createdAt = json['created_at'];
    desktopPoster = json['desktop_poster'];
    followCounts = json['follow_counts'];
    followed = json['followed'];
    id = json['id'];
    labels = List.castFrom<dynamic, String>(json['labels']);
    mobilePoster = json['mobile_poster'];
    musicCounts = json['music_counts'];
    name = json['name'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_on_playlist'] = addOnPlaylist;
    _data['artist'] = artist.toJson();
    _data['avatars'] = avatars;
    _data['created_at'] = createdAt;
    _data['desktop_poster'] = desktopPoster;
    _data['follow_counts'] = followCounts;
    _data['followed'] = followed;
    _data['id'] = id;
    _data['labels'] = labels;
    _data['mobile_poster'] = mobilePoster;
    _data['music_counts'] = musicCounts;
    _data['name'] = name;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class MostLikedMusics {
  MostLikedMusics({
    required this.addToPlaylist,
    required this.artists,
    required this.banner,
    required this.categories,
    required this.company,
    required this.createdAt,
    required this.description,
    required this.desktopPoster,
    required this.dislikeCount,
    required this.disliked,
    required this.followers,
    required this.id,
    required this.information,
    this.labels,
    required this.likeCount,
    required this.liked,
    required this.marked,
    required this.mobilePoster,
    required this.nameEn,
    required this.nameFa,
    required this.normalRbts,
    required this.playCount,
    required this.poem,
    required this.published,
    required this.publishedAt,
    required this.streamPath,
    required this.tags,
    required this.updatedAt,
  });
  late final bool addToPlaylist;
  late final List<Artists> artists;
  late final String banner;
  late final List<Categories> categories;
  late final Company company;
  late final String createdAt;
  late final String description;
  late final String desktopPoster;
  late final int dislikeCount;
  late final bool disliked;
  late final int followers;
  late final String id;
  late final List<dynamic> information;
  late final Null labels;
  late final int likeCount;
  late final bool liked;
  late final bool marked;
  late final String mobilePoster;
  late final String nameEn;
  late final String nameFa;
  late final List<NormalRbts> normalRbts;
  late final int playCount;
  late final String poem;
  late final bool published;
  late final String publishedAt;
  late final String streamPath;
  late final List<Tags> tags;
  late final String updatedAt;

  MostLikedMusics.fromJson(Map<String, dynamic> json){
    addToPlaylist = json['add_to_playlist'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    banner = json['banner'];
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    company = Company.fromJson(json['company']);
    createdAt = json['created_at'];
    description = json['description'];
    desktopPoster = json['desktop_poster'];
    dislikeCount = json['dislike_count'];
    disliked = json['disliked'];
    followers = json['followers'];
    id = json['id'];
    information = List.castFrom<dynamic, dynamic>(json['information']);
    labels = null;
    likeCount = json['like_count'];
    liked = json['liked'];
    marked = json['marked'];
    mobilePoster = json['mobile_poster'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    normalRbts = List.from(json['normal_rbts']).map((e)=>NormalRbts.fromJson(e)).toList();
    playCount = json['play_count'];
    poem = json['poem'];
    published = json['published'];
    publishedAt = json['published_at'];
    streamPath = json['stream_path'];
    tags = List.from(json['tags']).map((e)=>Tags.fromJson(e)).toList();
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_to_playlist'] = addToPlaylist;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['banner'] = banner;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['company'] = company.toJson();
    _data['created_at'] = createdAt;
    _data['description'] = description;
    _data['desktop_poster'] = desktopPoster;
    _data['dislike_count'] = dislikeCount;
    _data['disliked'] = disliked;
    _data['followers'] = followers;
    _data['id'] = id;
    _data['information'] = information;
    _data['labels'] = labels;
    _data['like_count'] = likeCount;
    _data['liked'] = liked;
    _data['marked'] = marked;
    _data['mobile_poster'] = mobilePoster;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['normal_rbts'] = normalRbts.map((e)=>e.toJson()).toList();
    _data['play_count'] = playCount;
    _data['poem'] = poem;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['stream_path'] = streamPath;
    _data['tags'] = tags.map((e)=>e.toJson()).toList();
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class NewestMusic {
  NewestMusic({
    required this.addToPlaylist,
    required this.artists,
    required this.banner,
    required this.categories,
    required this.company,
    required this.createdAt,
    required this.description,
    required this.desktopPoster,
    required this.dislikeCount,
    required this.disliked,
    required this.followers,
    required this.id,
    required this.information,
    required this.labels,
    required this.likeCount,
    required this.liked,
    required this.marked,
    required this.mobilePoster,
    required this.nameEn,
    required this.nameFa,
    required this.normalRbts,
    required this.playCount,
    required this.poem,
    required this.published,
    required this.publishedAt,
    required this.streamPath,
    required this.tags,
    required this.updatedAt,
  });
  late final bool addToPlaylist;
  late final List<Artists> artists;
  late final String banner;
  late final List<Categories> categories;
  late final Company company;
  late final String createdAt;
  late final String description;
  late final String desktopPoster;
  late final int dislikeCount;
  late final bool disliked;
  late final int followers;
  late final String id;
  late final List<dynamic> information;
  late final List<String> labels;
  late final int likeCount;
  late final bool liked;
  late final bool marked;
  late final String mobilePoster;
  late final String nameEn;
  late final String nameFa;
  late final List<NormalRbts> normalRbts;
  late final int playCount;
  late final String poem;
  late final bool published;
  late final String publishedAt;
  late final String streamPath;
  late final List<Tags> tags;
  late final String updatedAt;

  NewestMusic.fromJson(Map<String, dynamic> json){
    addToPlaylist = json['add_to_playlist'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    banner = json['banner'];
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    company = Company.fromJson(json['company']);
    createdAt = json['created_at'];
    description = json['description'];
    desktopPoster = json['desktop_poster'];
    dislikeCount = json['dislike_count'];
    disliked = json['disliked'];
    followers = json['followers'];
    id = json['id'];
    information = List.castFrom<dynamic, dynamic>(json['information']);
    labels = List.castFrom<dynamic, String>(json['labels']);
    likeCount = json['like_count'];
    liked = json['liked'];
    marked = json['marked'];
    mobilePoster = json['mobile_poster'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    normalRbts = List.from(json['normal_rbts']).map((e)=>NormalRbts.fromJson(e)).toList();
    playCount = json['play_count'];
    poem = json['poem'];
    published = json['published'];
    publishedAt = json['published_at'];
    streamPath = json['stream_path'];
    tags = List.from(json['tags']).map((e)=>Tags.fromJson(e)).toList();
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_to_playlist'] = addToPlaylist;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['banner'] = banner;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['company'] = company.toJson();
    _data['created_at'] = createdAt;
    _data['description'] = description;
    _data['desktop_poster'] = desktopPoster;
    _data['dislike_count'] = dislikeCount;
    _data['disliked'] = disliked;
    _data['followers'] = followers;
    _data['id'] = id;
    _data['information'] = information;
    _data['labels'] = labels;
    _data['like_count'] = likeCount;
    _data['liked'] = liked;
    _data['marked'] = marked;
    _data['mobile_poster'] = mobilePoster;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['normal_rbts'] = normalRbts.map((e)=>e.toJson()).toList();
    _data['play_count'] = playCount;
    _data['poem'] = poem;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['stream_path'] = streamPath;
    _data['tags'] = tags.map((e)=>e.toJson()).toList();
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class TopPlaylist {
  TopPlaylist({
    required this.addOnPlaylist,
    required this.artist,
    this.avatars,
    required this.createdAt,
    required this.desktopPoster,
    required this.followCounts,
    required this.followed,
    required this.id,
    required this.labels,
    required this.mobilePoster,
    required this.musicCounts,
    required this.name,
    required this.published,
    required this.publishedAt,
    required this.updatedAt,
  });
  late final bool addOnPlaylist;
  late final Artist artist;
  late final Null avatars;
  late final String createdAt;
  late final String desktopPoster;
  late final int followCounts;
  late final bool followed;
  late final String id;
  late final List<String> labels;
  late final String mobilePoster;
  late final int musicCounts;
  late final String name;
  late final bool published;
  late final String publishedAt;
  late final String updatedAt;

  TopPlaylist.fromJson(Map<String, dynamic> json){
    addOnPlaylist = json['add_on_playlist'];
    artist = Artist.fromJson(json['artist']);
    avatars = null;
    createdAt = json['created_at'];
    desktopPoster = json['desktop_poster'];
    followCounts = json['follow_counts'];
    followed = json['followed'];
    id = json['id'];
    labels = List.castFrom<dynamic, String>(json['labels']);
    mobilePoster = json['mobile_poster'];
    musicCounts = json['music_counts'];
    name = json['name'];
    published = json['published'];
    publishedAt = json['published_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['add_on_playlist'] = addOnPlaylist;
    _data['artist'] = artist.toJson();
    _data['avatars'] = avatars;
    _data['created_at'] = createdAt;
    _data['desktop_poster'] = desktopPoster;
    _data['follow_counts'] = followCounts;
    _data['followed'] = followed;
    _data['id'] = id;
    _data['labels'] = labels;
    _data['mobile_poster'] = mobilePoster;
    _data['music_counts'] = musicCounts;
    _data['name'] = name;
    _data['published'] = published;
    _data['published_at'] = publishedAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class Metadata {
  Metadata({
    required this.limit,
    required this.currentPage,
    required this.totalPages,
    required this.totalCounts,
    required this.sort,
  });
  late final int limit;
  late final int currentPage;
  late final int totalPages;
  late final int totalCounts;
  late final String sort;

  Metadata.fromJson(Map<String, dynamic> json){
    limit = json['limit'];
    currentPage = json['current_page'];
    totalPages = json['total_pages'];
    totalCounts = json['total_counts'];
    sort = json['sort'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['limit'] = limit;
    _data['current_page'] = currentPage;
    _data['total_pages'] = totalPages;
    _data['total_counts'] = totalCounts;
    _data['sort'] = sort;
    return _data;
  }
}