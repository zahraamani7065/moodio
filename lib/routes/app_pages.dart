import 'package:get/get.dart';
import 'package:moodio/Views/Screens/home_page.dart';
import 'package:moodio/controllers/main_controllers.dart';
import 'package:moodio/main.dart';

import 'package:moodio/routes/app_routes.dart';

import '../Views/Screens/play_list.dart';
import '../Views/Screens/profile_page.dart';
import '../Views/Screens/ringback_page.dart';
import '../Views/Screens/search_page.dart';
import '../binding/home_binding.dart';
import '../binding/main_binding.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
        name: Routes.main, page: () => MainScreen(), binding: MainBinding()),
    GetPage(
        name: Routes.HomePage, page: () => HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.RingBackPage, page: () => RingBackPage()),
    GetPage(name: Routes.SearchPage, page: () => SearchPage()),
    GetPage(
      name: Routes.PlayListPage,
      page: () => PlayListPage(),
    ),
    GetPage(
      name: Routes.ProfilePage,
      page: () => ProfilePage(),
    ),
  ];
}
