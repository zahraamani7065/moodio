

abstract class Routes{
  Routes._();
  static const main='/';
  static const HomePage ='/home';
  static const ProfilePage='/profile';
  static const PlayListPage='/playList';
  static const SearchPage='/Search';
  static const RingBackPage='/ringback';

}