import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moodio/Views/Screens/home_page.dart';
import 'package:moodio/Views/Screens/play_list.dart';
import 'package:moodio/Views/Screens/profile_page.dart';
import 'package:moodio/Views/Screens/ringback_page.dart';
import 'package:moodio/Views/Screens/search_page.dart';
import 'package:moodio/controllers/home_controller.dart';
import 'package:moodio/controllers/main_controllers.dart';
import 'package:moodio/routes/app_pages.dart';
import 'package:moodio/routes/app_routes.dart';
import 'package:moodio/views/widgets/buttom_navigation.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      builder: (context, child) => ResponsiveWrapper.builder(child,
          maxWidth: 1200,
          minWidth: 480,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(480, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background: Container(color: Color(0xFFF5F5F5))),
      theme: ThemeData(
        textTheme: const TextTheme(
          headline6: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white70),
          bodyText1: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white70),
          bodyText2: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 16,
              color: Colors.white38),
          subtitle1: TextStyle(
              fontWeight: FontWeight.normal, fontSize: 18, color: Colors.white),
        ),
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      initialRoute: Routes.main,
      getPages: AppPages.routes,
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    final mainController = Get.find<MainController>();
    return Scaffold(bottomNavigationBar: Obx(() {
      return buttomNavigation(
        selectedIndex: mainController.tabIndex.value,
        mainController: mainController,
        onTap: (int index) {
          mainController.changTabIndex(index);
        },
      );
    }), body: Obx(() {
      return IndexedStack(
        index: mainController.tabIndex.value,
        children: [
          const HomePage(),
          PlayListPage(),
          ProfilePage(),
          RingBackPage(),
          const SearchPage(),
        ],
      );
    }));
  }
}
