import 'package:get/get.dart';
import 'package:moodio/services/api_service.dart';
import 'package:moodio/models/home/home_data.dart';

class HomeController extends GetxController {
  var isLoading = true.obs;
  var data = AutoGenerate().obs;

  @override
  void onInit() {
    fetchData();
    super.onInit();
  }

  void fetchData() async {
    isLoading(true);
    try {
      var datas = await ApiServices().getData();
      data.value = datas as AutoGenerate;
    } finally {
      isLoading(false);
    }
  }
}
