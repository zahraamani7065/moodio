import 'dart:convert';

import 'package:moodio/constants/urls.dart';
import 'package:moodio/models/home/home_data.dart';

import '../constants/errors.dart';
import 'package:http/http.dart' as http;

class ApiServices {
  Future<AutoGenerate> getData() async {
    final response = await http.get(Uri.parse(AppUrls.homeDataURl));
    if (response.statusCode == 200) {
      return AutoGenerate.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(AppErrors.LoadDataEr);
    }
  }
}
